package com.shuyi.scaffold;

import com.shuyi.scaffold.config.mq.CustomInput;
import com.shuyi.scaffold.config.mq.CustomOutput;
import com.shuyi.scaffold.config.redis.CustomRedissonAutoConfigurationCustomizer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableFeignClients
//@EnableScheduling
//@EnableAsync
@MapperScan("com.shuyi.*.mapper")
@SpringBootApplication
@EnableBinding({Source.class, Sink.class, CustomOutput.class, CustomInput.class})
public class ScaffoldApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScaffoldApplication.class, args);
    }

    @Bean
    public CustomRedissonAutoConfigurationCustomizer redissonAutoConfigurationCustomizer() {
        return new CustomRedissonAutoConfigurationCustomizer("aaaa");
    }

}
