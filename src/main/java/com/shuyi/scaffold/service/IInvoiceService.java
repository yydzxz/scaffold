package com.shuyi.scaffold.service;

import com.shuyi.scaffold.entity.Invoice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账户系统单据 服务类
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
public interface IInvoiceService extends IService<Invoice> {

}
