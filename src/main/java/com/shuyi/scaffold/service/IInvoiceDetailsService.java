package com.shuyi.scaffold.service;

import com.shuyi.scaffold.entity.InvoiceDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 单据明细 服务类
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
public interface IInvoiceDetailsService extends IService<InvoiceDetails> {

}
