package com.shuyi.scaffold.service.impl;

import com.shuyi.scaffold.entity.Invoice;
import com.shuyi.scaffold.mapper.InvoiceMapper;
import com.shuyi.scaffold.service.IInvoiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户系统单据 服务实现类
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
@Service
public class InvoiceServiceImpl extends ServiceImpl<InvoiceMapper, Invoice> implements IInvoiceService {

}
