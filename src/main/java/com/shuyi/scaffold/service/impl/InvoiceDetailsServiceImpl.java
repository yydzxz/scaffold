package com.shuyi.scaffold.service.impl;

import com.shuyi.scaffold.entity.InvoiceDetails;
import com.shuyi.scaffold.mapper.InvoiceDetailsMapper;
import com.shuyi.scaffold.service.IInvoiceDetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 单据明细 服务实现类
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
@Service
public class InvoiceDetailsServiceImpl extends ServiceImpl<InvoiceDetailsMapper, InvoiceDetails> implements IInvoiceDetailsService {

}
