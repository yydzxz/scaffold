package com.shuyi.scaffold.mapper;

import com.shuyi.scaffold.entity.Invoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户系统单据 Mapper 接口
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
public interface InvoiceMapper extends BaseMapper<Invoice> {

}
