package com.shuyi.scaffold.mapper;

import com.shuyi.scaffold.entity.InvoiceDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 单据明细 Mapper 接口
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
public interface InvoiceDetailsMapper extends BaseMapper<InvoiceDetails> {

}
