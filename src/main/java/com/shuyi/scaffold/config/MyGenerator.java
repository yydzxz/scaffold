package com.shuyi.scaffold.config;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Property;
import java.util.Collections;

/**
 * @author yangyidian
 * @date 2022/06/18
 **/
public class MyGenerator {
    public static void main(String[] args) {
        String packageName = "com.shuyi.scaffold";
        String projectName = "./";

        FastAutoGenerator
//            .create("jdbc:mysql://127.0.0.1:3306/account?characterEncoding=UTF-8", "root", "mysqlmysql")
            .create("jdbc:mysql://127.0.0.1:3306/account_statement_dev?characterEncoding=UTF-8", "shuyi_account", "a376FH)oWqXtM)9vHEf")
            .globalConfig(builder -> {
                builder
                    .outputDir(projectName + "/src/main/java")
                    .author("书亦")
                    .dateType(DateType.TIME_PACK)
//                    .commentDate("yyyy-MM-dd")
                    .commentDate(()->"2022-06-21");

            })
            .packageConfig(builder -> {
                builder.parent(packageName)
                    .entity("entity")
                    .service("service")
                    .serviceImpl("service.impl")
                    .mapper("mapper")
                    .controller("controller")
                    .pathInfo(Collections.singletonMap(OutputFile.xml, projectName + "/src/main/resources/mapper"));
            })
            .strategyConfig(builder -> {
                builder
                    .addInclude("acc_invoice", "acc_invoice_details")
//                    .likeTable(new LikeTable("acc_"))
                    .addTablePrefix("acc_")
                    .controllerBuilder()
                    .enableRestStyle()
                    .entityBuilder()
                    .enableLombok()
                    .fileOverride()
                    .entityBuilder()
                    .idType(IdType.ASSIGN_ID)
                    .addTableFills(new Property("updateTime", FieldFill.UPDATE))
                    .versionColumnName("version")
                    .logicDeleteColumnName("delete_status");
            })
            .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
            .execute();
    }

}
