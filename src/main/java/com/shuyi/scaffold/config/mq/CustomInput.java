package com.shuyi.scaffold.config.mq;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
public interface CustomInput {

    String myInput = "myInput";

    @Input("myInput")
    SubscribableChannel consume();
}
