package com.shuyi.scaffold.config.mq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
public interface CustomOutput {

    @Output("myOutput")
    MessageChannel output();
}
