package com.shuyi.scaffold.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author yangyidian
 * @date 2022/08/24
 **/
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "schedule")
public class ScheduleConfig {

    private DemoScheduleConfig demoScheduleConfig;

    @Getter
    @Setter
    public static class DemoScheduleConfig{
        private Boolean enabled;
    }
}
