package com.shuyi.scaffold.config;

import com.shuyi.scaffold.exception.ScaffoldBizException;
import com.shuyi.shuyicommon.dto.base.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author yangyidian
 * @date 2022/08/24
 **/
@Slf4j
@RestControllerAdvice
public class ScaffoldBizExceptionHandler {

    @ExceptionHandler(ScaffoldBizException.class)
    public Response<String> handleDuplicatedOutBizNoAndTypeException(ScaffoldBizException e){
        log.error(e.getMessage(), e);
        return Response.error("SCAFFOLD-ERROR-CODE", e.getMessage());
    }
}
