package com.shuyi.scaffold.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 账户系统单据
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
@Getter
@Setter
@TableName("acc_invoice")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String tenantCode;

    private String appCode;

    private String clientCode;

    private Long clientId;

    /**
     * 单据号
     */
    private String no;

    /**
     * 1支出 2退款 3收入
     */
    private Integer type;

    /**
     * 外部流水号
     */
    private String outBizNo;

    /**
     * 外部流水号类型 比如1:订单号 2:凭证号,这个是外部系统自己决定的,账户系统不作限制
     */
    private Integer outBizType;

    /**
     * 外部流水总金额
     */
    private BigDecimal outBizAmount;

    /**
     * 外部流水时间
     */
    private LocalDateTime outBizTime;

    /**
     * 1prepare 2confirm 3cancel
     */
    private Integer status;

    /**
     * 该单据退款状态 0没有退款 1全部退款 2部分退款
     */
    private Integer refundStatus;

    /**
     * 乐观锁版本号
     */
    @Version
    private Integer version;

    private LocalDateTime createTime;

    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    private String creator;

    @TableLogic
    private Integer deleteStatus;


}
