package com.shuyi.scaffold.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 单据明细
 * </p>
 *
 * @author 书亦
 * @since 2022-06-21
 */
@Getter
@Setter
@TableName("acc_invoice_details")
public class InvoiceDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String tenantCode;

    private String appCode;

    private String clientCode;

    private Long invoiceItemId;

    private Long clientId;

    /**
     * 账户id
     */
    private Long accountId;

    /**
     * 账户类型 余额,优惠券 物料金
     */
    private Integer accountType;

    /**
     * 单据id
     */
    private Long invoiceId;

    /**
     * 单据号
     */
    private String invoiceNo;

    /**
     * 外部流水号
     */
    private String outBizNo;

    /**
     * 外部流水号类型: 订单号,凭证号
     */
    private Integer outBizType;

    /**
     * 流水金额
     */
    private BigDecimal amount;

    /**
     * 实际流水金额(目前主要针对优惠券)
     */
    private BigDecimal amountActual;

    /**
     * 明细类型: 支付,退款, 收入
     */
    private Integer invoiceType;

    private Long couponId;

    /**
     * 收支方向 1支出 2收入
     */
    private Integer direction;

    /**
     * 明细描述(物料金来源等)
     */
    private String description;

    private LocalDateTime createTime;

    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    private String creator;

    @TableLogic
    private Integer deleteStatus;


}
