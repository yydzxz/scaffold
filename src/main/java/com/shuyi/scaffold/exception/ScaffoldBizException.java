package com.shuyi.scaffold.exception;

import com.shuyi.shuyicommon.exception.BusinessException;

/**
 * 项目的自定义业务异常
 * @author yangyidian
 * @date 2022/08/24
 **/
public class ScaffoldBizException extends BusinessException {

    public ScaffoldBizException() {
        super();
    }

    public ScaffoldBizException(String message) {
        super(message);
    }
}
