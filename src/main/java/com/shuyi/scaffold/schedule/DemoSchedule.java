package com.shuyi.scaffold.schedule;

import com.shuyi.scaffold.config.ScheduleConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author yangyidian
 * @date 2022/08/24
 **/
@Slf4j
@Component
@RefreshScope
@AllArgsConstructor
public class DemoSchedule {

    private ScheduleConfig scheduleConfig;

    public void call(){
        if(!scheduleConfig.getDemoScheduleConfig().getEnabled()){
            return;
        }
    }

}
