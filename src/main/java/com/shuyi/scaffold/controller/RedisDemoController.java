package com.shuyi.scaffold.controller;

import com.shuyi.shuyicommon.service.CacheService;
import com.shuyi.shuyicommon.service.LockService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
@RestController
@RequestMapping("/redis/demo")
@AllArgsConstructor
public class RedisDemoController {

    private CacheService cacheService;

    private LockService lockService;

}
