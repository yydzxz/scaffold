package com.shuyi.scaffold.controller;

import com.shuyi.shuyicommon.dto.base.Response;
import com.shuyi.shuyicommon.service.CacheService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangyidian
 * @date 2022/08/26
 **/
@RestController
@RequestMapping("/cache/demo")
@AllArgsConstructor
public class CacheDemoServiceController {

    private CacheService cacheService;

    @PostMapping("/set")
    public Response<String> set(String key, String value){
        cacheService.set(key, value);
        return Response.success();
    }

}
