package com.shuyi.scaffold.controller;

import com.shuyi.shuyicommon.dto.base.Response;
import com.shuyi.shuyicommon.service.LockService;
import java.util.concurrent.TimeUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangyidian
 * @date 2022/08/26
 **/
@RestController
@RequestMapping("/lock/demo")
@AllArgsConstructor
public class LockServiceDemoController {

    private LockService lockService;

    @GetMapping("/isLock")
    public Response<Boolean> isLock(String key){
        return Response.success(lockService.isLock(key));
    }

    @PostMapping("/lock")
    public Response<Boolean> lock(String key, Integer expire){
        return Response.success(lockService.lock(key, expire, TimeUnit.SECONDS));
    }
}
