package com.shuyi.scaffold.controller;

import lombok.AllArgsConstructor;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangyidian
 * @date 2022/08/24
 **/
@RestController
@RequestMapping("/demo")
@AllArgsConstructor
public class DemoController {

    private RedissonClient redissonClient;

    @PostMapping("/set")
    public void set(String key, Object value){
        redissonClient.getBucket(key).set(value);
    }
}
