package com.shuyi.scaffold.controller;

import com.shuyi.scaffold.mq.producer.DemoProducer;
import com.shuyi.scaffold.mq.producer.DemoProducer2;
import com.shuyi.shuyicommon.dto.base.Response;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
@RestController
@RequestMapping("/mq/demo")
@AllArgsConstructor
public class MQDemoController {

    private DemoProducer demoProducer;

    private DemoProducer2 demoProducer2;

    @PostMapping("/produce")
    public Response<String> produce(String messageType, String message){
        demoProducer.pushSomeMessage(messageType, message);
        return Response.success();
    }


    @PostMapping("/produce2")
    public Response<String> produce2(String messageType, String message){
        demoProducer2.pushSomeMessage(messageType, message);
        return Response.success();
    }
}
