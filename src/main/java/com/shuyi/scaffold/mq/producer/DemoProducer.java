package com.shuyi.scaffold.mq.producer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
@Slf4j
@Component
public class DemoProducer {

    @Autowired
    private Source source;

    public void pushSomeMessage(String messageType, Object message){
        log.info("发送消息: messageType: {}, message: {}", messageType, message);
        source.output()
            .send(MessageBuilder.withPayload(message).build());
    }
}
