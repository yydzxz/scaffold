package com.shuyi.scaffold.mq.producer;

import com.shuyi.scaffold.config.mq.CustomInput;
import com.shuyi.scaffold.config.mq.CustomOutput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
@Slf4j
@Component
public class DemoProducer2 {

    @Autowired
    private CustomOutput customOutput;

    public void pushSomeMessage(String messageType, Object message){
        log.info("CustomOutput发送消息: messageType: {}, message: {}", messageType, message);
        customOutput.output()
            .send(MessageBuilder.withPayload(message).build());
    }
}
