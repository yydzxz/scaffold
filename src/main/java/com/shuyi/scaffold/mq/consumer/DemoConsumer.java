package com.shuyi.scaffold.mq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

/**
 * @author yangyidian
 * @date 2022/08/25
 **/
@Slf4j
@Component
public class DemoConsumer {

    @StreamListener(Sink.INPUT)
    public void consume(String message) {
        log.info("收到消息 {}" , message);
    }
}
