## 脚手架项目


redis demo
spring cloud stream demo
异常继承 common包


.
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── shuyi
│   │   │           └── scaffold
│   │   │               ├── app #聚合服务
│   │   │               ├── config #配置
│   │   │               │   ├── mq #消息队列配置
│   │   │               │   ├── mybatis #mybatis配置
│   │   │               │   └── redis #redis配置
│   │   │               ├── constant #常量
│   │   │               ├── controller
│   │   │               ├── converter #VO DTO PO Entity转换
│   │   │               ├── entity #
│   │   │               ├── enums # 枚举
│   │   │               ├── exception # 异常
│   │   │               ├── manager # 第三方调用
│   │   │               │   └── ydh # 封装调用亦订货例子
│   │   │               │       ├── client
│   │   │               │       ├── request
│   │   │               │       └── response
│   │   │               ├── mapper
│   │   │               ├── mq # 消息队列相关
│   │   │               │   ├── consumer
│   │   │               │   └── producer
│   │   │               ├── schedule #定时任务
│   │   │               └── service
│   │   │                   └── impl
│   │   └── resources
│   │       └── mapper
│   └── test
│       └── java
│           └── com
│               └── shuyi
│                   └── scaffold


66 directories

